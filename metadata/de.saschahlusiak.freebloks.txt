Categories:Games
License:GPLv2
Author Name:Sascha Hlusiak
Author Email:mail@saschahlusiak.de
Web Site:http://www.saschahlusiak.de/freebloks-3d-android/
Source Code:https://github.com/shlusiak/Freebloks-Android
Issue Tracker:https://github.com/shlusiak/Freebloks-Android/issues

Auto Name:Freebloks
Summary:Board game
Description:
Strategy board game similar to the famous board game Blokus.
.

Repo Type:git
Repo:https://github.com/shlusiak/Freebloks-Android.git

Build:0.5.2,67
    commit=foss-0.5.2
    gradle=yes
    rm=libs/*,stuff/
    buildjni=yes

Build:0.5.3,68
    commit=foss-0.5.3
    gradle=yes
    rm=libs/*,stuff/
    buildjni=yes

Auto Update Mode:Version foss-%v
Update Check Mode:RepoManifest/foss
Current Version:0.5.3
Current Version Code:68
