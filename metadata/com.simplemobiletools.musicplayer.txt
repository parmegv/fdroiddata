Categories:Multimedia
License:Apache2
Web Site:
Source Code:https://github.com/SimpleMobileTools/Simple-Music-Player
Issue Tracker:https://github.com/SimpleMobileTools/Simple-Music-Player/issues
Changelog:https://github.com/SimpleMobileTools/Simple-Music-Player/blob/HEAD/CHANGELOG.md

Auto Name:Simple Music Player
Summary:Basic music player with customizable widget
Description:
It takes every song from your device that is more than 20 seconds long, without
the ability to filter by artist etc. The app can be used for renaming the song
title, artist or filename. You can control it easily both from the status bar
notification or with hardware button on your headset.

It contains a customizable widget for your home screen, where you can change the
font color, or the alpha and the color of the background. The widget can be of
size 4x1 or 4x2.
.

Repo Type:git
Repo:https://github.com/SimpleMobileTools/Simple-Music-Player

Build:1.6,7
    commit=48abb7981a0273f583b495c44d11f11536afcc5d
    subdir=app
    gradle=yes

Build:1.8,8
    commit=1.8
    subdir=app
    gradle=yes

Build:1.9,9
    commit=1.9
    subdir=app
    gradle=yes

Build:1.10,10
    commit=1.10
    subdir=app
    gradle=yes

Build:1.11,11
    commit=1.11
    subdir=app
    gradle=yes

Build:1.12,12
    commit=1.12
    subdir=app
    gradle=yes

Build:1.14,14
    commit=1.14
    subdir=app
    gradle=yes

Build:1.15,15
    commit=1.15
    subdir=app
    gradle=yes

Build:1.16,16
    commit=1.16
    subdir=app
    gradle=yes

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:1.16
Current Version Code:16
