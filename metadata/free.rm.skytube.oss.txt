AntiFeatures:NonFreeNet
Categories:Multimedia,Internet
License:GPLv3
Web Site:
Source Code:https://github.com/ram-on/SkyTube
Issue Tracker:https://github.com/ram-on/SkyTube/issues
Changelog:https://github.com/ram-on/SkyTube/releases

Auto Name:SkyTube
Summary:A simple and feature-rich YouTube player
Description:
SkyTube is a YouTube player that allows you to:

* explore Featured and Most Popular videos,
* browse YouTube channels,
* play YouTube videos,
* view video comments,
* search videos, music and channels

... all at the tip of your fingers.

More features will be added in the near future.
.

Repo Type:git
Repo:https://github.com/ram-on/SkyTube

Build:1.0 OSS,1
    commit=v1.0
    subdir=app
    gradle=oss

Maintainer Notes:
* Current versions include app/libs/*.jar. Remove them.
* No AUM applies, since version != tag.
.

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.0 OSS
Current Version Code:1
